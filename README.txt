Assorted CMU tools for interacting with the IHMC simulator (written by Roger Carff) for the ASIST project.

------------------------------------------------------------------------------

Quickstart:

• ensure the simulator is running, and you are in a directory containing
  both the asist_model.py file and an experiments subdirectory containing
  at least Easy.json

• pip install -r requirements.txt

• python asist_model.py random

This should run random_model once, and print some summary results.

------------------------------------------------------------------------------

The principal file here is asist_model.py, a framework and several models for interacting
with the simulator, described more fully below. Other things stored here include:

• wrapper.py, a Python wrapper providing a simple TCP interface to the simulator
• asist-support.lisp, a Lisp client for interating with the above
• random-model.lisp, a sample Lisp model using the above
• a pair of systemd config files for running the simulator and wrapper
• mqttlogger.py, a simple MQTT logger
• requirements.txt, the Python requirements for asist_model.py
• mapl.lisp, used to generate representations of the map from an experiment definition

------------------------------------------------------------------------------

The file asist_model.py provides a framework for writing models to interact with
the simulator, a pair of initial models, and a main function for running models.

The primary entry point is the class Simulation. The asist_model module can be
imported to give access to this, or the file can be modified and run from
the command line. Typically a Simulation object is used as a context manager:

with Simulation(<arguments>) as exp:
  <code goes here for the model, using the variable exp>

For most purposes the only argument that will need to be supplied to the Simuation
is the experiment name.

When a Simulation object is initialized an MQTT connection to the simulator is
created, and the various initial handshakes and so on are performed to initialize
a trial of the experiment named and loaded from the experiments subdirectory.
Code within the with block can then interact with the simulator by calling methods
on the Simulation object. When the with block is left the connection is lost.

The most useful methods and instance variables of Simulation include the following.
In these descriptions exp is an instance of Simulation.

• exp.state: a dict containing the current state from the simulator; None after
the end of the game is reached.

• exp.choices: a dict containing the available choices from the simulator; None after
the end of the game is reached.

• exp.result: a dict containing information about the game just played; None before
the end of the game is reached.

• exp.details: a dict containing information about the design and initial state
of the experiment being run; it contains the following entries — note that for
those that change of the course of a trial these are the initial values:
    distance_dog_can_sense_victims
    durability_cost_of_clearing_one_debris
    energy_levels
    energy_used_to_clear_one_debris
    fire_extinguisher_level
    fire_extinguisher_levels
    for_triaging_critical
    for_triaging_non_critical
    health_levels
    health_used_to_go_through_fire
    map_of_building   (a Boolean indicting whether or not it is shown)
    name              (the name of the experiment)
    percent_of_extinguisher_used_for_one_fire
    pick_axe_durability
    pick_axe_durability_levels
    search_and_rescue_dog
    show_damage_indicator
    show_location_xy
    show_score
    show_time
    time_to_clear_one_debris
    time_to_move_one_space
    time_to_put_out_one_fire
    time_to_triage_critical
    time_to_triage_non_critical
    time_until_critical_victims_die
    total_time_to_search

• exp.follow_path(p): send a message to the simulator directing it to follow path p.
Returns a Boolean that is True iff the game is not yet ended.

• exp.location(id=None): returns the location with the given id, which defaults to
the current location.

• exp.victims(id=None): returns a dict with the number of victims at the location
with the given id, which defaults to the current location.

• exp.triage(critical, non_critical): send a message to the simulator directing it
to triage the specified numbers of critical and non_critical victims. Returns a
Boolean that is True iff the game has not yet ended.

• exp.destination(path): Returns the location at the other end of path, which must
be one of the paths current available to be traversed from the current location.

Undoubtedly further methods will need to be added as we track the evolution and
increasing complexity of the simulation.

------------------------------------------------------------------------------

If desired, it is possible to run models defined in asist_model.py from the command
line. There are two models currently defined:

• random: this moves randomly, and always triages all victims found.

• v2actup: this is a simple PyACTUp that attempts to be a little smarter.

To run a model from the command line do

python asist_model.py <options> <model>

The <model> should be a name (currently either “random” or “v2actup”) which
has been registered with the framework. Registering a name matches it to a
class name, which class should be a subclass of the Model class. Such a
subclass should override the run() method, which takes one argument, a
instance of a Simulation, and calls it to execute the model. See the source
for examples. Note that such a Model object can retain state between invocations,
if desired.

The most important of the <options> is --experiment (or -e), used to provide the
name of an experiment to run. It defaults to Easy. This option can be provided multiple
times, and the model will be run on all the provided experiments, in order.

The --repeat (or -n) option can be used to run the model repeatedly, after which
it computes means, maxima and minima of the summary information it provides, separately
for each experiment if multiple experiments are provided.

The --reload option will force rereading of an experiment definition file, and should
be used if that definition is edited.

The --noise, --decay, --mismatch and --temperature options can be used to modify
memory parameters for ACT-UP models.

The --debug, --trace and --paths options provide more verbose output of various kinds.

The --host and --port options provide connection parameters for the simulator.
