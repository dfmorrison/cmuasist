(load (merge-pathnames "asist-support" *load-pathname*))

(defun run-random-model (&key (experiment asist:+easy-experiment+) (host "atlas.psy.cmu.edu"))
  "Executes a random model against an experiment, defaulting to 'Easy'.
At each step it prints out the current state and choices, and selects and follows a
random path from those available. At the conclusion it prints the results object."
  (asist:with-experiment (state choices experiment :host host)
    (loop (progn
            (pprint state)
            (unless choices
              (format t "~2%;; *** Finished~2%")
              (return))
            (terpri)
            (pprint choices)
            (let* ((paths (cdr (assoc :paths choices)))
                   (choice (cdr (assoc :id (nth (random (length paths)) paths)))))
              (format t "~2%;; *** Following path ~A~2%" choice)
              (multiple-value-setq (state choices)
                (asist:decision :follow--path choice)))))))
