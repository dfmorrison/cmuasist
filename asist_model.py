import click
from collections import defaultdict
from contextlib import AbstractContextManager
from datetime import datetime
from itertools import count
import json
import logging
import matplotlib.pyplot as plt
import os
import paho.mqtt.client as mqtt
import pathlib
from pprint import pprint
import pyactup                  # must be version 1.0.7 or later
import queue
import random
import uuid
import time
from tqdm import tqdm

DEFAULT_PORT = 1883
DEFAULT_HOST = "127.0.0.1"
DEFAULT_EXPERIMENT_NAME = "Easy"
EXPERIMENT_DIRECTORY = "experiments"

logging.basicConfig(level=logging.INFO)

show_traffic = False

def timestamp():
    return f"{datetime.utcnow().isoformat()}Z"

def suuid():
    return str(uuid.uuid4())


total_received = 0
time_waiting = 0
total_time = 0

class Simulation(AbstractContextManager):

    def __init__(self,
                 experiment=DEFAULT_EXPERIMENT_NAME,
                 trial=suuid(),
                 host=DEFAULT_HOST,
                 port=DEFAULT_PORT,
                 reload=False,
                 **ignore):
        self.experiment = experiment
        self.trial = trial
        self.host = host
        self.port = port
        self.reload = reload
        self.experiment_id = None
        self.msg_q = queue.SimpleQueue()
        self.client = mqtt.Client(type(self).__name__)
        self.client.on_message = self.do_on_message
        self.result = None
        self.state = None
        self.choices = None
        self.experiment_details = None
        logging.debug("Created %s", self)

    def __enter__(self):
        self.client.connect(self.host, self.port)
        logging.debug("Connected to %s:%s", self.host, self.port)
        self.subscribe("IHMCSimASIST/response")
        self.subscribe("IHMCSimASIST/observation/state")
        self.subscribe("IHMCSimASIST/observation/choices")
        self.subscribe("IHMCSimASIST/observation/result")
        self.client.loop_start()
        logging.debug("Looping started")
        self.load_experiment()
        self.start_trial()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.client.loop_stop()
        logging.debug("Looping stopped")
        # test the type of the exception, and return normally if end of simulation

    def subscribe(self, topic):
        self.client.subscribe(topic)
        logging.debug("Subscribed to %s", topic)

    def do_on_message(self, client, user_data, msg):
        global total_received
        logging.debug("JSON Message received: %s", msg.payload.decode("utf-8"))
        self.msg_q.put(json.loads(msg.payload))
        total_received += 1

    def retrieve(self):
        global time_waiting
        start = time.time()
        result = self.msg_q.get()
        time_waiting += (time.time() - start)
        if show_traffic:
            print("\n<<<")
            pprint(result)
        logging.debug("Retrieving Python message %s", result)
        return result


    def send(self, data, subtopic=None, ts=None):
        data["header"] = {"version": "0.2",
                          "message_type": "observation" if subtopic else "control",
                          "timestamp": ts or timestamp()}
        message = json.dumps(data)
        topic = f"IHMCSimASIST/observation/{subtopic}" if subtopic else "IHMCSimASIST/control"
        logging.debug("Sending %s message as JSON: %s", topic, message)
        self.client.publish(topic, message)
        if show_traffic:
            print("\n>>>")
            pprint(message)

    def load_experiment(self):
        self.send({"msg": {"command": "list", "version": "2.0", "id": suuid()}})
        response = self.retrieve()
        if not response["msg"]["success"]:
            raise RuntimeError("Attempt to select experiment list failed")
        data = response.get("data", {})
        for id in data:
            if data[id].casefold() == self.experiment.casefold():
                self.experiment_id = id
        if not self.experiment_id:
            self.experiment_id = suuid()
        elif not self.reload:
            return
        for path in (pathlib.Path(__file__).parent.absolute() / EXPERIMENT_DIRECTORY).rglob("*.json"):
            logging.debug("Examining %s to see if it matches %s", path, self.experiment)
            with open(path) as f:
                definition = json.loads(f.read())
                logging.debug("Experiment named %s (%s)", definition["name"], definition["name"].casefold())
                if definition["name"].casefold() == self.experiment.casefold():
                    logging.debug("Loading %s from %s", self.experiment, path)
                    self.experiment = definition["name"]
                    eid = definition.get("id", self.experiment_id)
                    if eid:
                        if not self.experiment_id:
                            self.experiment_id = eid
                        elif self.experiment_id != eid:
                            logging.warning("Experiment id mismatch %s, %s",
                                            eid, self.experiment_id)
                            self.experiment_id = eid
                    elif self.experiment_id:
                        definition["id"] = self.experiment_id
                    else:
                        self.experiment_id = suuid()
                        logging.warning("No experiment id, assigning %s", self.experiment_id)
                        definition["id"] = self.experiment_id
                    self.send({"msg": {"command": "add", "version": "2.0", "id": suuid()}, "data": definition})
                    response = self.retrieve()
                    if not response["msg"]["success"]:
                        raise RuntimeError(f"Failed to load experiment {self.experiment}")
                    self.make_experiment_details(definition)
                    return
        raise RuntimeError(f"Can't find experiment named {self.experiment}")

    def start_trial(self):
        logging.debug("Starting trial %s of %s (%s)",
                      self.trial, self.experiment, self.experiment_id)
        self.send({"msg": {"command": "init",
                           "id": suuid(),
                           "experiment_id": self.experiment_id,
                           "trial_id": self.trial}})
        response = self.retrieve()
        if not response["msg"]["success"]:
            raise RuntimeError(
                f"Attempt to initialize trial {self.trial} of {self.experiment} failed")
        self.send({"msg": {"command": "start", "id": suuid(), "trial_id": self.trial}})
        response = self.retrieve()
        if not response["msg"]["success"]:
            raise RuntimeError(
                f"Attempt to start trial {self.trial} of {self.experiment} failed")
        self.update()

    def update(self):
        self.choices = None
        self.state = None
        message = self.retrieve()
        if message["msg"].get("sub_type") == "result":
            self.result = message["data"]
            return False
        if message["msg"].get("sub_type") != "state":
            raise RuntimeError(f"Expected state update, got {message}")
        self.state = message["data"]
        message = self.retrieve()
        if message["msg"].get("sub_type") != "choices":
            raise RuntimeError(f"Expected choices update, got {message}")
        self.choices = message["data"]
        return True

    def decide(self, **data):
        data["trial_id"] = self.trial
        data["choice_id"] = self.choices["id"]
        ts = timestamp()
        self.send({"msg": {"timestamp": ts,
                           "source": "python_client",
                           "sub_type": "decision",
                           "version": "0.2"},
                   "data": data},
                  subtopic="decision",
                  ts=ts)
        return self.update()

    def follow_path(self, p):
        """Sends a message to the simulator directing it to follow path p.
           Returns a Boolean that is True iff the game has not yet ended."""
        logging.debug("Following path %s", p["id"])
        return self.decide(follow_path=p["id"])

    def location(self, id=None):
        """Returns the location with the given id, which defaults to the current location."""
        if not id:
            id = self.choices["location_id"]
        for loc in self.choices.get("locations") or []:
            if loc["id"] == id:
                return loc

    def victims(self, id=None):
        """Returns a dict with the number of victims at the location with the given id,
           which defaults to the current location."""
        return self.location(id).get("victims")

    def triage(self, critical, non_critical):
        """Sends a message to the simulator directing it to triage the specified numbers
           of critical and non_critical victims.
           Returns a Boolean that is True iff the game has not yet ended."""
        logging.debug("Triaging %s critical and %s non-critical", critical, non_critical)
        # print(critical, non_critical, self.state["elapsed_sim_time"], self.details["time_until_critical_victims_die"])
        return self.decide(triage_critical=critical, triage_non_critical=non_critical)

    def destination(self, path):
        """Returns the location at the other end of path, which must be one of the paths
           current available to be traversed from the current location."""
        here = self.choices["location_id"]
        loc = path["loc_id_1"]
        return path["loc_id_2"] if loc == here else loc

    @property
    def details(self):
        if not self.experiment_details:
            self.send({"msg": {"command": "get",
                               "version": "2.0",
                               "id": suuid(),
                               "experiment_id": self.experiment_id}})
            response = self.retrieve()
            if not response["msg"]["success"]:
                raise RuntimeError(
                    f"Attempt to retrieve details of {self.experiment_id} failed")
            self.make_experiment_details(response["data"])
        return self.experiment_details

    def make_experiment_details(self, definition):
        result = {"name": definition["name"]}
        for sub in "visuals,limits,tools,costs,points".split(","):
            for key, value in definition["parameters"][sub].items():
                if result.get(key):
                    logging.warning("Duplicate detail key %s", key)
                result[key] = value
        self.experiment_details = result


class Model:

    def __init__(self, **kwargs):
        pass

    def run(self, experiment):
        raise RuntimeError("The run() method must be overridden to use the Model class")

    def analyize(self, experiment, results):
        pass


models = {}

def register_model(name, cls):
    if models.get(name):
        logging.warning("There is already a model named %s defined, it will be overwritten", name)
    models[name] = cls

@click.command()
@click.argument("model")
@click.option("--experiment", "-e", default=[DEFAULT_EXPERIMENT_NAME], multiple=True, help="Name(s) of experiment(s) to run")
@click.option("--repeat", "-n", default=1, help="Number of times to run the model on the experiment(s)")
@click.option("--reload", is_flag=True, help="Force reloading of experiment definition")
@click.option("--noise", "-N", default=0.25, help="Declarative memory noise parameter value")
@click.option("--decay", "-D", default=0.5, help="Declarative memory decay parameter value")
@click.option("--mismatch", "-M", default=1, help="Declarative memory mismatch penalty value")
@click.option("--temperature", "-T", default=None, help="Declarative memory temperature parameter value")
@click.option("--host", default=DEFAULT_HOST, help="Host of simulator to connect to")
@click.option("--port", default=DEFAULT_PORT, help="Port on which to connect to simulator")
@click.option("--debug", "-d", is_flag=True, help="Print verbose debugging information")
@click.option("--trace", "-t", is_flag=True, help="Print MQTT messages read and written")
@click.option("--paths", "-p", is_flag=True, help="Show the paths traversed for each model run")
@click.option("--variable", "-v", help="Pass a parameter to the model initialization")
def main(**kwargs):
    global show_traffic, total_time
    if kwargs["debug"]:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.info("Debugging enabled")
    if kwargs["trace"]:
        show_traffic = True
        logging.debug("Tracing enabled")
    model = models.get(kwargs["model"])
    if not model:
        raise RuntimeError(f"No model %s is registered", kwargs["model"])
    experiments = kwargs["experiment"]
    results = defaultdict(list)
    progress = tqdm(total=(kwargs["repeat"] * len(experiments)))
    for r in range(kwargs["repeat"]):
        for i, e in zip(count(), experiments):
            m = model(**kwargs)
            kwargs["experiment"] = e
            logging.debug("About to run %s (%d, %d)", e, i, r)
            with Simulation(**kwargs) as exp:
                start_time = time.time()
                m.run(exp)
                m.analyze(exp.result, e, i)
                total_time = (time.time() - start_time)
                results[(i, e)].append(exp.result)
                progress.update()
    for i, e in zip(count(), experiments):
        analyze(e, results[(i, e)], kwargs["paths"])
    print(total_received, "messages,", total_time, time_waiting, time_waiting / total_time)

RESULT_KEYS = ("score critical_found critical_total triaged_critical "
               + "non_critical_found non_critical_total triaged_non_critical "
               + "deceased distance_traveled debris_cleared "
               + "energy fire_extinguisher_level fires_put_out health "
               + "pick_axe_durability non_critical_triage_btd_probability").split()

def analyze(name, results, show_paths):
    n = len(results)
    sums = defaultdict(int)
    maxima = defaultdict(int)
    minima = defaultdict(lambda: 1e100)
    non_critical_triage_btd_probabilities = [0] * 12
    # For now we're assuming there's only a single player
    for r in results:
        for k, v in r["player_results"][0].items():
            r[k] = v
    for result in results:
        for key in RESULT_KEYS:
            if key in result:
                sums[key] += result[key]
                maxima[key] = max(maxima[key], result[key])
                minima[key] = min(minima[key], result[key])
        count = len(result["locations_visited"])
        sums["locations_visited"] += count
        maxima["locations_visited"] = max(maxima["locations_visited"], count)
        minima["locations_visited"] = min(minima["locations_visited"], count)
        if "non_critical_triage_btd_probability" in result:
            p = result["non_critical_triage_btd_probability"]
            if p >= 1:
                non_crtical_triage_btd_probabilities[11] += 1
            elif p <= 0:
                non_crtical_triage_btd_probabilities[0] += 1
            else:
                for i in range(10):
                    if p < i * 0.1:
                        non_critical_triage_btd_probabilities[i + 1] += 1
                        break
    print(f"\n{name} experiment\nMeans, maxima, minima over {n} iterations: ")
    for key in RESULT_KEYS:
        if key in sums:
            print(f"  {key}:  {(sums[key]/n):.1f},   {maxima[key]},   {minima[key]}")
    if show_paths:
        print("Paths traversed:")
        for d in result["decisions_made"]:
            p = d.get("follow_path")
            if p:
                print("  ", p)
    if sum(non_critical_triage_btd_probabilities):
        print("binned noncritical triage BTD probabilities:",
              non_critical_triage_btd_probabilities,
              sum(non_critical_triage_btd_probabilities))
    else:
        for r in results:
            print(r["score"])

class RandomModel(Model):

    def run(self, exp):
        pprint(exp.details)
        while not exp.result:
            v = exp.victims()
            if v:
                exp.triage(v.get("critical", 0), v.get("non_critical", 0))
            if not exp.result:  # game might have while triaging
                pprint(exp.state)
                exp.follow_path(random.choice(exp.choices["paths"]))


register_model("random", RandomModel)


class BlendException (Exception):
    pass


def model_blend(mem, attr, **kwargs):
    result = mem.blend(attr, **kwargs)
    if result is None:
        raise BlendException()
    return result


no_distance_warned = False

def get_distance(p):
    global no_distance_warned
    id = p["id"]
    if "distance" not in p and not no_distance_warned:
        logging.warning("One or more paths have no explicit distance, assuming 1")
        no_distance_warned = True
    return p.get("distance", 1)


class ACTUpBase(Model):

    @staticmethod
    def linear_similarity(x, y):
        assert x >= 0 and y >= 0
        return 1 - abs(x - y) / max(x, y)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.mem = pyactup.Memory(noise=kwargs["noise"],
                                  decay=kwargs["decay"],
                                  mismatch=kwargs["mismatch"],
                                  temperature=kwargs["temperature"])

    def run(self, exp):
        while not exp.result:
            self.make_triage_choice(exp)
            self.make_movement_choice(exp)

    def make_triage_choice(self, e):
        raise NotImplementedError("make_triage_choice() must be overridden")

    def make_movement_choice(self, e):
        raise NotImplementedError("make_movement_choice() must be overridden")


class ACTUpTriage(ACTUpBase):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        pyactup.set_similarity_function(ACTUpBase.linear_similarity,
                                        "critical_victims", "non_critical_victims")
        self.non_critical_found_btd = 0
        self.non_critical_triaged_btd = 0

    def make_triage_choice(self, e):
        v = e.victims()
        if not v:
            return
        critical = v.get("critical", 0)
        non_critical = v.get("non_critical", 0)
        best_outcome = 0
        best_actions = []
        start_time = e.state["elapsed_sim_time"]
        start_points = e.state["score"]
        is_btd = e.state["elapsed_sim_time"] < e.details["time_until_critical_victims_die"]
        if is_btd:
            self.non_critical_found_btd += non_critical
        try:
            for c in range(critical + 1):
                for n in range(non_critical + 1):
                    args = { "critical_victims": critical,
                             "critical_triaged": c,
                             "non_critical_victims": non_critical,
                             "non_critical_triaged": n }
                    points = model_blend(self.mem, "points", **args)
                    time = model_blend(self.mem, "time", **args)
                    rate = points / time if time > 0 else 0
                    if rate == best_outcome:
                        best_actions.append((c, n))
                    elif rate > best_outcome:
                        best_outcome = rate
                        best_actions = [(c, n)]
        except BlendException:
            if is_btd:
                non_critical = non_critical and random.randrange(non_critical)
            best_actions = [(critical, non_critical)]
        ct, nt  = random.choice(best_actions)
        if is_btd:
            self.non_critical_triaged_btd += nt
        # if e.state["elapsed_sim_time"] < e.details["time_until_critical_victims_die"]:
        #     print(critical, "/", ct, "  |||  ",non_critical, "/", nt)
        # else:
        #     print("        |||  ",non_critical, "/", nt)
        e.triage(ct, nt)
        if e.result:
            return                  # game over
        time = e.state["elapsed_sim_time"] - start_time
        self.mem.learn(critical_victims=c,
                       non_critical_victims=n,
                       critical_triaged=ct,
                       non_critical_triaged=nt,
                       points=(e.state["score"] - start_points),
                       time=time,
                       advance=max(time, 1))

    def analyze(self, results, experiment, i):
        results["non_critical_triage_btd_probability"] = (
            self.non_critical_triaged_btd / self.non_critical_found_btd)

class ACTUpNavigation(ACTUpBase):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        pyactup.set_similarity_function(ACTUpBase.linear_similarity, "distance")

    NOVELTY_BONUS = 30

    def make_movement_choice(self, e):
        if e.result:                # the triage actions ended the game
            return
        here = e.choices["location_id"]
        best_outcome = 0
        best_paths = []
        start_time = e.state["elapsed_sim_time"]
        self.mem.advance()
        for p in e.choices["paths"]:
            dest = e.destination(p)
            if not self.mem.retrieve(visited=dest):
                self.mem.learn(visited=dest, novelty=ACTUpNavigation.NOVELTY_BONUS, advance=0)
        self.mem.advance()
        for p in e.choices["paths"]:
            dest = e.destination(p)
            novelty = model_blend(self.mem, "novelty", visited=dest)
            victims = e.victims(dest)
            if victims:
                args = { "critical_victims": victims.get("critical", 0),
                         "non_critical_victims": victims.get("non_critical", 0) }
            else:
                args = {}
            points = self.mem.blend("points", **args) or 0
            time = self.mem.blend("time", **args) or 1
            transit = self.mem.blend("transit", distance=get_distance(p)) or 1
            time += transit
            rate = points / time + novelty
            if rate == best_outcome:
                best_paths.append(p)
            elif rate > best_outcome:
                best_outcome = rate
                best_paths = [p]
        path = random.choice(best_paths)
        e.follow_path(path)
        if e.result:                # game over
            return
        transit = e.state["elapsed_sim_time"] - start_time
        self.mem.learn(distance=get_distance(path),
                       transit=transit,
                       visited=here,
                       advance=max(transit, 1),
                       novelty=0)


class AllACTUpModel(ACTUpTriage, ACTUpNavigation):
    pass

register_model("all-actup", AllACTUpModel)


class DeterministicTriage(ACTUpBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._time_transition_noted = False

    def make_triage_choice(self, e):
        v = e.victims()
        if not v:
            return
        critical = v.get("critical", 0)
        non_critical = v.get("non_critical", 0)
        best_outcome = 0
        best_actions = []
        start_time = e.state["elapsed_sim_time"]
        start_points = e.state["score"]
        if start_time < e.details["time_until_critical_victims_die"]:
            n = self.predeath_triage(e, critical, non_critical)
        else:
            if not self._time_transition_noted:
                self.note_time_transition()
                self._time_transition_noted = True
            n = non_critical
            e.triage(critical, non_critical)
        if e.result:
            return                  # game over
        time = e.state["elapsed_sim_time"] - start_time
        # We still learn stuff that might impact an ACT-Up navigation choice
        self.mem.learn(critical_victims=critical,
                       non_critical_victims=n,
                       critical_triaged=critical,
                       non_critical_triaged=n,
                       points=(e.state["score"] - start_points),
                       time=time,
                       advance=max(time, 1))

    def predeath_triage(self, e, critical, non_critical):
        raise NotImplementedError("predeath_triage() must be overridden")

    def note_time_transition(self):
        pass


class NonCriticalTriage(DeterministicTriage):

    def predeath_triage(self, e, critical, non_critical):
        e.triage(critical, non_critical)
        return non_critical


class CriticalTriage(DeterministicTriage):

    def predeath_triage(self, e, critical, non_critical):
        e.triage(critical, 0)
        return 0


class CriticalTriageRevisit(CriticalTriage):

    def note_time_transition(self):
        self._visitations = {}


class VariableTriage(DeterministicTriage):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.triage_rate = float(kwargs.get("variable") or 0)

    def predeath_triage(self, e, critical, non_critical):
        triage_these = 0
        for i in range(non_critical):
            if self.triage_rate and random.random() < self.triage_rate:
                triage_these += 1
        e.triage(critical, triage_these)
        return triage_these


class DeterministicNavigation(ACTUpBase):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._visitations = {}
        self._bounces = []

    def run(self, exp):
        super().run(exp)
        # print("Visited", len(self._visitations), "locations")

    def visitation(self, id):
        result = self._visitations.get(id)
        if result:
            return result
        result = []
        self._visitations[id] = result
        return result

    def make_movement_choice(self, e):
        if e.result:                # the triage actions ended the game
            return
        here = e.choices["location_id"]
        if self._bounces and self._bounces[0] == here:
            bounces = self._bounces[1:]
        else:
            bounces = []
            self._bounces = [here]
        candidates = [(p, self.visitation(e.destination(p)))
                      for p in reversed(e.choices["paths"]) if p not in bounces]
        unvisited = list(filter(lambda x: not x[1], candidates))
        if unvisited:
            candidates = unvisited
            best = self.best_unvisited(e, candidates)
        else:
            n = min(len(x[1]) for x in candidates)
            candidates = list(filter(lambda x: len(x[1]) == n, candidates))
            best = min(candidates, key=lambda x: x[1][0])
        bounces.append(best[0])
        e.follow_path(best[0])
        if e.result:            # game over
            return
        self.visitation(e.destination(best[0])).insert(0, e.state["elapsed_sim_time"])

    @staticmethod
    def total_victims(e, path):
        location = e.destination(path)
        victims = e.victims(location)
        if victims:
            return victims.get("critical", 0) + victims.get("non_critical", 0)
        else:
            return 0

    def best_unvisited(self, e, candidates):
            n = max(DeterministicNavigation.total_victims(e, x[0]) for x in candidates)
            candidates = list(filter(lambda x: HybridModel.total_victims(e, x[0]) == n, candidates))
            return candidates[0]


class HybridModel(ACTUpTriage, DeterministicNavigation):
    pass

register_model("hybrid", HybridModel)


class NonCriticalDeterminisicModel(NonCriticalTriage, DeterministicNavigation):
    pass

register_model("non-critical", NonCriticalDeterminisicModel)


class CriticalDeterministicModel(CriticalTriage, DeterministicNavigation):
    pass

register_model("critical", CriticalDeterministicModel)

class CriticalDeterministicModel(CriticalTriageRevisit, DeterministicNavigation):
    pass

register_model("critical-revisit", CriticalDeterministicModel)


class VariableTriageModel(VariableTriage, DeterministicNavigation):
    pass

register_model("variable", VariableTriageModel)


if __name__ == '__main__':
    main()
