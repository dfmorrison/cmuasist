import click
import json
import logging
import paho.mqtt.client as mqtt
import socketserver
import threading
import time

DEFAULT_PORT = 8889
DEFAULT_ASIST_HOST = "127.0.0.1"
DEFAULT_ASIST_PORT = 1883

client = mqtt.Client("PyACTUp-ASIST-wrapper")

client.is_connected = False

current_handler = None

handler_lock = threading.Lock()

def do_on_message(c, user_data, msg):
    global current_handler, handler_lock
    message = msg.payload.decode("utf-8")
    logging.debug("Message received from ASIST server: %s", message)
    handler_lock.acquire()
    try:
        if current_handler:
            current_handler.write(message)
    finally:
        handler_lock.release()

client.on_message = do_on_message

def do_on_connect(c, userdata, flags, rc):
    client.is_connected = True
    logging.debug("MQTT connected (%s, %s)", flags, rc)

client.on_connect = do_on_connect

def do_on_disconnect(c, userdata, rc):
    client.is_connected = False
    logging.info("MQTT disconnected (%s)", rc)

client.on_disconnect = do_on_disconnect

class WrapperHandler(socketserver.StreamRequestHandler):

    _host = DEFAULT_ASIST_HOST
    _port = DEFAULT_ASIST_PORT
    _terminate = False

    def setup(self):
        global current_handler, handler_lock
        super().setup()
        logging.info("Connecting to %s:%s", self._host, self._port)
        client.connect(self._host, self._port)
        client.subscribe("IHMCSimASIST/response")
        logging.debug("Subscribed to IHMCSimASIST/response")
        for sub in "state choices result".split():
            client.subscribe(f"IHMCSimASIST/observation/{sub}")
            logging.debug("Subscribed to IHMCSimASIST/observation/%s", sub)
        handler_lock.acquire()
        try:
            if current_handler:
                raise RuntimeError("Multiple handlers, hopeless confusion!")
            current_handler = self
        finally:
            handler_lock.release()
        client.loop_start()
        logging.debug("Loop started")
        # takes a little while for the connection message to fire
        for i in range(100):
            if client.is_connected:
                break
            time.sleep(0.01)

    def handle(self):
        try:
            while client.is_connected:
                message = self.rfile.readline().decode().strip()
                logging.debug("Read %s", message)
                if not message:
                    break
                try:
                    is_control = json.loads(message)["header"]["message_type"] == "control"
                except Exception as e:
                    logging.error("%s: %s", e, message)
                    break
                topic = "IHMCSimASIST/control" if is_control else "IHMCSimASIST/observation/decision"
                logging.debug("Publishing %s: %s", topic, message)
                client.publish(topic, message)
        except KeyboardInterrupt as e:
            logging.debug("Keyboard interrupt")
            WrapperHandler._terminate = True
        except Exception as e:
            logging.error(str(e))

    def write(self, s):
        logging.debug("Writing %s", s)
        try:
            self.wfile.write(s.encode())
            self.wfile.write(b"\n")
        except BrokenPipeError as e:
            logging.debug("Attempt to write %s to a closed connection (%s)", s, e)
            return False
        return True

    def finish(self):
        global current_handler, handler_lock
        handler_lock.acquire()
        try:
            current_handler = None
        finally:
            handler_lock.release()
        # leave a little while for pending messages to drain
        time.sleep(0.1)
        if client.is_connected:
            client.disconnect()
        client.loop_stop()
        super().finish()


@click.command()
@click.option("--port", "-p", default=DEFAULT_PORT, type=int)
@click.option("--asist-host", "-H", default=DEFAULT_ASIST_HOST)
@click.option("--asist-port", "-P", default=DEFAULT_ASIST_PORT, type=int)
@click.option("--verbose", "-v", is_flag=True)
def main(port, asist_host, asist_port, verbose):
    """Runs a TCP server listening on the given port and delegating ASIST commands
       and decisions to the MQTT server running on the given asist_host and asist_port,
       and then returning results to the original port."""
    logging.basicConfig(level=(logging.DEBUG if verbose else logging.INFO))
    if verbose:
        logging.debug("Wrapper started in verbose mode")
    WrapperHandler._host = asist_host
    WrapperHandler._port = asist_port
    with socketserver.TCPServer(("", port), WrapperHandler) as server:
        while not WrapperHandler._terminate:
            server.handle_request()


if __name__ == '__main__':
    main(auto_envvar_prefix="WRAPPER")
