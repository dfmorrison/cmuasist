# Copyright (c) 2020 Carnegie Mellon University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# This is a minimal logger of traffic on an MQTT bus. It listens for messages on one or
# more topics, and writes the matching traffic to a CSV file. To run it typically do
# something like
#
#     python mqttlogger.py <topic>
#
# Multiple topics can be specified, and the usual MQTT topic wildcards can be used. For
# example
#
#     python mqttlogger.py 'IHMCSimASIST/response' 'IHMCSimASIST/observation/#'
#
# By default it writes to standard out, but can be directed to write to a file with the
# --file command line option. The host and port used to connect to the MQTT broker can be
# specified with the --host and --port command line options.
#
# Output is written as CSV, so embedded quotation marks will typically be doubled to
# escape them. This may be particularly important to bear in mind when the data being sent
# is JSON. Also note that output to the CSV file is typically buffered and may not be
# flushed to the output file until mqttlogger is stopped (typically with control-C).
#
# This has only been tested on Linux, and requires Python 3, as well as the Paho MQTT
# client library and the click command line processing library. If necessary these
# can be installed with
#
#     pip install paho-mqtt click

import click
import csv
from datetime import datetime
import os
import paho.mqtt.client as mqtt
import sys

broker_host = "127.0.0.1"
broker_port = 1883
message_encoding = "utf-8"
log_writer = None

def do_on_message(client, userdata, msg):
    write_record(datetime.now().isoformat(),
                 userdata and userdata.decode(message_encoding),
                 msg.topic,
                 msg.payload.decode(message_encoding))

def write_record(timestamp, topic, userdata, payload):
    log_writer.writerow([timestamp, topic, userdata, payload])

def log_topics(file, topics, header=True):
    global log_writer
    log_writer = csv.writer(file)
    if header:
        write_record("timestamp", "userdata", "topic", "payload")
    client = mqtt.Client("mqttlogger")
    client.on_message = do_on_message
    client.connect(broker_host, broker_port)
    for topic in topics:
        # wildcard topics can be helpful here
        client.subscribe(topic)
    client.loop_forever()

@click.command()
@click.option("-f", "--file", type=click.Path(),
              help="File to write or append to; use standard out if not supplied")
@click.option("-h", "--host", default=broker_host,
              help="Host name or IP address of MQTT broker")
@click.option("-p", "--port", default=broker_port, type=int,
              help="Port to use to connect to MQTT broker")
@click.option("--encoding", default=message_encoding,
              help="Encoding used for MQTT message payloads, UTF-8 if not supplied")
@click.argument("topics", nargs=-1, required=True)
def main(topics, file, host, port, timeout, encoding):
    global broker_host, broker_port, message_encoding
    broker_host = host
    broker_port = port
    message_encoding = encoding
    if file:
        exists = os.path.isfile(file)
        with open(file, "a", newline="") as csv_file:
            log_topics(csv_file, topics, not exists)
    else:
        log_topics(sys.stdout, topics)


if __name__ == "__main__":
    main()
