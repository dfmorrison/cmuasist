import matplotlib.pyplot as plt
from statistics import mean

MAP = "Falcon"

MODEL = "Normal"

#DATA = "160 160 110 110 110 110 100 130 50 120 100 80 100 0 110 100 100 50 70 100 90 90 80 110 90 100 90 100 110 20"

# maxval = {"Sparky": 400, "Falcon": 500}[MAP]
#
# data = list(map(int, DATA.split()))
#
# x = list(range(0, maxval + 10, 10))
# y = [0]*len(x)
# for n in data:
#     y[int(n / 10)] += 1
# plt.bar(x, y, width=8)
# plt.yticks(range(0, max(y) + 1))
# plt.xlabel("points earned")
# plt.title(f"{len(data)} iterations, {MAP} map, {MODEL} model, mean = {round(mean(data))}")
# plt.show()



# DATA = "320 320 310 340 320 330 340 340 340 340 340"
# DATA = "300 291 294 301 308 315 313 324 328 335 340"

# XDATA = "0.00625 0.0125 0.025 0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4"
# YDATA = "440 440 440 410 380 340 220 140 60 10 0"
#
# xdata = list(map(float, XDATA.split()))
# ydata = list(map(int, YDATA.split()))

DATA = "200 200 200 200 200 200 200 200 200 200 200"

data = list(map(int, DATA.split()))

x = [0.1 * i for i in range(11)]

plt.plot(x, data)
# plt.xscale("log")
# plt.xticks(xdata)
plt.ylabel("score")
plt.ylim(0, 500)
# plt.xlabel("time_to_move_one_space")
# plt.title(f"Falcon v0.5, parameterized by time_to_move_one_space")
plt.xlabel("fracton of non-critical victims triaged if found before death time")
plt.title(f"Falcon v0.5, time_to_move_one_space=0.4, parameterized by pre-death triage rate")
plt.show()
