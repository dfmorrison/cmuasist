(ql:quickload '(:iterate :cl-json :cl-csv :local-time))

(defpackage :asist-map
  (:use :common-lisp :iterate :json :cl-csv :local-time)
  (:export #:convert-map))

(in-package :asist-map)

(defparameter *pathname-defaults* (make-pathname :type "json"
                                                 :defaults *default-pathname-defaults*))
(eval-when (:load-toplevel)
  (setf *pathname-defaults* (merge-pathname *pathname-defaults* *load-pathname*)))

(declaim (ftype function print-node))

(progn
  #0=(defstruct (node (:print-object print-node))
       "The neighbors slot contains a list of pairs. the car of each pair is an adjoining
          node and its cdr is the distance to that node.
        The victims slot contains a property list mapping the keywords :non-critical and
          :critical to the numbers of such victims at this node. Absence or nil are
          equivalent to zero."
       id area x y (neighbors nil) (victims nil))
  (defparameter +defstruct-form+ '#0#))

(progn
  #0=(defun print-node (node stream)
       (cond (*print-readably*
              (let ((*print-circle* t))
                (format stream "#S(NODE :ID ~S :AREA ~S :X ~S :Y ~S :NEIGHBORS ~S :VICTIMS ~S)~%"
                        (node-id node) (node-area node) (node-x node) (node-y node)
                        (node-neighbors node) (node-victims node))))
             (*print-escape*
              (print-unreadable-object (node stream :type t :identity t)
                (format stream "~A (~{~A~^, ~})"
                        (node-id node) (mapcar #'car (node-neighbors node)))))
             (t (princ (node-id node) stream))))
  (defparameter +print-node-form+ '#0#))

(defun convert-map (file)
  (let* (in-file
         (json (with-open-file (in (merge-pathnames file *pathname-defaults*))
                 (setf in-file (truename in))
                 (decode-json in)))
         (out-filename (format nil "~A-map" (pathname-name file)))
         (start (cdr (assoc :starting--location
                            (cdr (assoc :player--info
                                        (cdr (assoc :parameters json)))))))
         (nodes (make-hash-table :test 'equal))
         (node-ids (make-array 1 :adjustable t :fill-pointer t :initial-contents (list start)))
         matrix)
    (iter (for loc :in (cdr (assoc :locations json)))
          (for id := (cdr (assoc :id loc)))
          (for startp := (equal id start))
          (setf (gethash id nodes)
                (cons (make-node :id id
                                 :area (cdr (assoc :area--id loc))
                                 :x (cdr (assoc :x loc))
                                 :y (cdr (assoc :y loc))
                                 :victims (subst :non-critical :non--critical
                                                 (cdr (assoc :victims loc))))
                      (if startp 0 (length node-ids))))
          (unless startp
            (vector-push-extend id node-ids)))
    (setf matrix (make-array `(,(length node-ids) ,(length node-ids)) :initial-element 0))
    (iter (for path :in (cdr (assoc :paths json)))
          (for (loc1 . i1) := (gethash (cdr (assoc :loc--id--1 path)) nodes))
          (for (loc2 . i2) := (gethash (cdr (assoc :loc--id--2 path)) nodes))
          (when (eq loc1 loc2)
            (warn "Vacuous path ~A" (cdr (assoc :id path)))
            (next-iteration))
          (for d := (or (cdr (assoc :distance path)) 1))
          (assert (> d 0))
          (push (cons loc2 d) (node-neighbors loc1))
          (push (cons loc1 d) (node-neighbors loc2))
          (setf (aref matrix i1 i2) d)
          (setf (aref matrix i2 i1) d))
    (macrolet ((write-file (type &body body)
                 `(with-open-file (out (make-pathname :name out-filename
                                                      :type ,type
                                                      :defaults *pathname-defaults*)
                                       :direction :output
                                       :if-exists :supersede)
                    ,@body
                    (truename out))))
      (values (write-file "lisp"
                          (format out ";; ~:D locations, from ~A, converted ~A~2%~@{~S~2%~}"
                                  (length node-ids)
                                  in-file
                                  (format-timestring nil (now))
                                  '(declaim (ftype function print-node))
                                  +defstruct-form+
                                  +print-node-form+)
                          (let ((*print-readably* t) (*print-circle* t) (*print-pretty* nil))
                            (prin1 `(defparameter *asist-map* ,(car (gethash start nodes))) out)
                            (let ((*print-pretty* t))
                              (format out "~@{~2%~S~}~%"
                                      `(defparameter *asist-map-ids* ,node-ids)
                                      `(defparameter *asist-map-matrix* ,matrix)))))
              (write-file "csv"
                          (write-csv-row (cons nil (coerce node-ids 'list)) :stream out)
                          (dotimes (i (length node-ids))
                            (write-csv-row (cons (aref node-ids i)
                                                 (iter (for j :from 0 :below (length node-ids))
                                                       (collect (aref matrix i j))))
                                           :stream out)))
              (length node-ids)))))
